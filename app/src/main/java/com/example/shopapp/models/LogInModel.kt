package com.example.shopapp.models

import com.google.gson.annotations.SerializedName

data class LogInModel(
    @SerializedName("user_id")
    val userId: Int,
    val token: String
)
