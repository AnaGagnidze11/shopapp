package com.example.shopapp.models

data class SignUpModel(
    val OK: Boolean,
    val registered: Boolean
)