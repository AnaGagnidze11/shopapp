package com.example.shopapp.models

data class ReturnedError (val ok: Boolean, val error: String)