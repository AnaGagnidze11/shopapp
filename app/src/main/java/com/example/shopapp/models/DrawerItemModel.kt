package com.example.shopapp.models

data class DrawerItemModel(val id: Int, val itemName: String)
