package com.example.shopapp.screens.home.bottom_nav.wall

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopapp.models.Post
import com.example.shopapp.network.ResultControl
import com.example.shopapp.repository.post.PostsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class WallViewModel @Inject constructor(private val postsRepository: PostsRepository): ViewModel() {

    private var postsLiveData = MutableLiveData<ResultControl<List<Post>>>()
    val _postsLiveData: LiveData<ResultControl<List<Post>>> = postsLiveData
        fun getPosts(){
            viewModelScope.launch {
                withContext(Dispatchers.IO){
                    postsLiveData.postValue(postsRepository.getPosts())
                }
            }
        }

}