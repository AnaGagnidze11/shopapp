package com.example.shopapp.screens.home.bottom_nav.order

import com.example.shopapp.BaseFragment
import com.example.shopapp.databinding.OrderFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderFragment : BaseFragment<OrderFragmentBinding>(OrderFragmentBinding::inflate) {
    override fun setUpFragment() {

    }
}