package com.example.shopapp.screens.auth.log_in

import android.app.Dialog
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentLogInBinding
import com.example.shopapp.extensions.*
import com.example.shopapp.network.ResultControl
import com.example.shopapp.user_data.UserInfo
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

typealias strings = R.string
typealias colors = R.color

@AndroidEntryPoint
class LogInFragment : BaseFragment<FragmentLogInBinding>(FragmentLogInBinding::inflate) {

    private val logInViewModel: LogInViewModel by viewModels()



    override fun setUpFragment() {
        binding.tilEmail.isEndIconVisible = false
        binding.pgbLogIn.gone()
        setTexts()
        init()
    }



    private fun setTexts() {
        binding.txtVSignUp.setSpannedString(
            arrayOf(
                getString(strings.new_user),
                getString(strings.sign_up),
                getString(strings.here)
            ),
            arrayOf(colors.text_clr_gray, colors.main_color, colors.text_clr_gray)
        )

        binding.btnSignIn.authButton.text = getString(strings.sign_in)
    }
    private fun init(){
        binding.btnSignIn.authButton.setOnClickListener {
            logInInfo()
        }

        binding.txtVSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_signUpFragment)
        }

        binding.tipEmail.doOnTextChanged { text, start, before, count ->
            emailValidation(text.toString())
        }
    }

    private fun emailValidation(text: String){
        binding.tilEmail.isEndIconVisible = text.checkEmail()
    }

    private fun logInInfo(){
        val email = binding.tipEmail.text.toString()
        val password = binding.tipPassword.text.toString()
        if (email.isNotEmpty() && password.isNotEmpty()){
            if (email.checkEmail()){
                logInViewModel.logIn(email, password)
                observe()
            }else{
                showDialog(getString(strings.invalid_email))
            }
        }else{
            showDialog(getString(strings.fill_fields))
        }
    }

    private fun showDialog(desc: String){
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog_layout)
        dialog.findViewById<TextView>(R.id.description).text = desc
        dialog.findViewById<Button>(R.id.closingButton).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }

    private fun observe(){
        logInViewModel._logInLiveData.observe(viewLifecycleOwner, {
            when(it.status){
                ResultControl.Status.SUCCESS -> {
                    logInViewModel.saveSession(binding.cbRememberMe.isChecked)
                    it.data?.let { it1 -> logInViewModel.saveToken(it1.token) }
                    binding.pgbLogIn.goneIf(it.loading)
                    findNavController().navigate(R.id.action_logInFragment_to_homeFragment)
                    Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                }
                ResultControl.Status.ERROR -> {
                    binding.pgbLogIn.goneIf(it.loading)
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
                }
                ResultControl.Status.LOADING -> {binding.pgbLogIn.goneIf(it.loading)}
            }
        })
    }

}