package com.example.shopapp.screens.auth.log_in

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopapp.models.LogInModel
import com.example.shopapp.network.ResultControl
import com.example.shopapp.repository.LogInRepository
import com.example.shopapp.user_data.UserInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@HiltViewModel
class LogInViewModel @Inject constructor(private val logInRepository: LogInRepository, private val userInfo: UserInfo): ViewModel() {

    private val logInLiveData = MutableLiveData<ResultControl<LogInModel>>()
    val _logInLiveData: LiveData<ResultControl<LogInModel>> = logInLiveData

    fun logIn(email:String, password: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val result = loggingIn(email, password)
                logInLiveData.postValue(result)
            }
        }

    }


    private suspend fun loggingIn(email:String, password: String): ResultControl<LogInModel> {
        return logInRepository.logIn(email, password)
    }

    fun saveSession(inSession: Boolean){
        userInfo.saveSession(inSession)
    }

    fun saveToken(token: String){
        userInfo.saveToken(token)
    }
}