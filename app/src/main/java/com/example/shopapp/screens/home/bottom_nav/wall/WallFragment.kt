package com.example.shopapp.screens.home.bottom_nav.wall

import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shopapp.BaseFragment
import com.example.shopapp.adapter.PostsRecyclerViewAdapter
import com.example.shopapp.databinding.WallFragmentBinding
import com.example.shopapp.network.ResultControl
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WallFragment : BaseFragment<WallFragmentBinding>(WallFragmentBinding::inflate) {

    private val wallViewModel: WallViewModel by viewModels()
    private lateinit var adapter: PostsRecyclerViewAdapter
    override fun setUpFragment() {
        initPosts()
        setListeners()
        observe()
    }


    private fun initPosts(){
        wallViewModel.getPosts()
        adapter = PostsRecyclerViewAdapter()
        binding.postsRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.postsRecyclerView.adapter = adapter
    }

    private fun setListeners(){
        binding.swipeRefresh.setOnRefreshListener {
            wallViewModel.getPosts()
        }
    }

    private fun observe(){
        wallViewModel._postsLiveData.observe(viewLifecycleOwner, {
            binding.swipeRefresh.isRefreshing = it.loading
            when(it.status){
                ResultControl.Status.SUCCESS -> {
                    it.data?.let { posts -> adapter.getPosts(posts.toMutableList()) }
                }
                ResultControl.Status.ERROR -> {

                }

                ResultControl.Status.LOADING -> {

                }
            }
        })
    }


}