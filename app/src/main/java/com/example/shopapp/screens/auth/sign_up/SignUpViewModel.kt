package com.example.shopapp.screens.auth.sign_up

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shopapp.models.SignUpModel
import com.example.shopapp.network.ResultControl
import com.example.shopapp.repository.LogInRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(private val logInRepository: LogInRepository): ViewModel() {


    private val signUpLiveData = MutableLiveData<ResultControl<SignUpModel>>()
    val _signUpLiveData: LiveData<ResultControl<SignUpModel>> = signUpLiveData

    fun signUp(email:String, password: String, fullname: String){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val result = signingUp(email, password, fullname)
                signUpLiveData.postValue(result)
            }
        }

    }


    private suspend fun signingUp(email:String, password: String, fullname: String): ResultControl<SignUpModel> {
        return logInRepository.signUp(email, password, fullname)
    }
}