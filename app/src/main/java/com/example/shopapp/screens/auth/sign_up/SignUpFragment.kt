package com.example.shopapp.screens.auth.sign_up

import android.app.Dialog
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentSignUpBinding
import com.example.shopapp.extensions.*
import com.example.shopapp.network.ResultControl
import dagger.hilt.android.AndroidEntryPoint

typealias strings = R.string
typealias colors = R.color
@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignUpBinding>(FragmentSignUpBinding::inflate) {

    private val signUpViewModel: SignUpViewModel by viewModels()

    override fun setUpFragment() {
        binding.tilEmail.isEndIconVisible = false
        binding.pgbSignUp.gone()
        setTexts()
        init()
    }

    private fun setTexts() {
        binding.txtVSignIn.setSpannedString(
            arrayOf(
                getString(strings.already_a_member),
                getString(strings.log_in)
            ),
            arrayOf(colors.text_clr_gray, colors.main_color)
        )

        binding.btnSignUp.authButton.text = getString(strings.sign_in)
    }
    private fun init(){
        binding.btnSignUp.authButton.setOnClickListener {
            signUpInfo()
        }

        binding.txtVSignIn.setOnClickListener {
            findNavController().navigate(R.id.action_signUpFragment_to_logInFragment)
        }

        binding.tipEmail.doOnTextChanged { text, start, before, count ->
            emailValidation(text.toString())
        }
    }

    private fun emailValidation(text: String){
        binding.tilEmail.isEndIconVisible = text.checkEmail()
    }

    private fun signUpInfo(){
        val email = binding.tipEmail.text.toString()
        val fullName = binding.tipFullName.text.toString()
        val password = binding.tipPassword.text.toString()
        val repeatPassword = binding.tipRepeatPassword.text.toString()
        if (email.isNotEmpty() && fullName.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if (email.checkEmail()){
                if (password == repeatPassword){
                    signUpViewModel.signUp(email, password, fullName)
                    observe()
                }else{
                    showDialog(getString(strings.passwords_error))
                }
            }else{
                showDialog(getString(strings.invalid_email))
            }
        }else{
            showDialog(getString(strings.fill_fields))
        }
    }

    private fun showDialog(desc: String){
        val dialog = Dialog(requireContext())
        dialog.setUp(R.layout.dialog_layout)
        dialog.findViewById<TextView>(R.id.description).text = desc
        dialog.findViewById<Button>(R.id.closingButton).setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }


    private fun observe(){
        signUpViewModel._signUpLiveData.observe(viewLifecycleOwner, {
            when(it.status){
                ResultControl.Status.SUCCESS -> {
                    Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                    binding.pgbSignUp.goneIf(it.loading)
                    findNavController().navigate(R.id.action_signUpFragment_to_logInFragment)
                }
                ResultControl.Status.ERROR -> {
                    binding.pgbSignUp.goneIf(it.loading)
                    Toast.makeText(requireContext(), "${it.message}", Toast.LENGTH_SHORT).show()
                }
                ResultControl.Status.LOADING -> {binding.pgbSignUp.goneIf(it.loading)}
            }
        })
    }


}