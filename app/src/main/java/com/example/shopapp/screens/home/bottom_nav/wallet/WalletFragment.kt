package com.example.shopapp.screens.home.bottom_nav.wallet

import com.example.shopapp.BaseFragment
import com.example.shopapp.databinding.WallFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WalletFragment : BaseFragment<WallFragmentBinding>(WallFragmentBinding::inflate) {
    override fun setUpFragment() {

    }

}