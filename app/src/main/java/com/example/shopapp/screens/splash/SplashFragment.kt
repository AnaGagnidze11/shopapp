package com.example.shopapp.screens.splash

import android.animation.Animator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.databinding.FragmentSplashBinding
import com.example.shopapp.user_data.UserInfo
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>(FragmentSplashBinding::inflate) {

    private val splashViewModel: SplashViewModel by viewModels()

    override fun setUpFragment() {
        setUpAnimation()
    }

    private fun setUpAnimation(){
        binding.lottieAnimation.addAnimatorListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                openNextFragment()
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }

        })
    }

    private fun openNextFragment(){
        if (splashViewModel.hasSession())
            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
        else
            findNavController().navigate(R.id.action_splashFragment_to_logInFragment)
    }
}