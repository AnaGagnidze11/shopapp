package com.example.shopapp.screens.splash

import androidx.lifecycle.ViewModel
import com.example.shopapp.user_data.UserInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(private val userInfo: UserInfo): ViewModel() {

    fun hasSession() = userInfo.hasSession()
}