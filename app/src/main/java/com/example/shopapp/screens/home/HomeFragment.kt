package com.example.shopapp.screens.home


import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shopapp.BaseFragment
import com.example.shopapp.R
import com.example.shopapp.adapter.DrawerRecyclerViewAdapter
import com.example.shopapp.databinding.FragmentHomeBinding
import com.example.shopapp.models.DrawerItemModel
import dagger.hilt.android.AndroidEntryPoint

typealias strings = R.string

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private lateinit var adapter: DrawerRecyclerViewAdapter

    override fun setUpFragment() {
        setUpBottomNav()
        setUpDrawerMenu()
    }

    private fun setUpBottomNav() {
        val navController =
            childFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
        binding.navView.setupWithNavController(navController.navController)
    }

    private fun setUpDrawerMenu() {
        adapter = DrawerRecyclerViewAdapter(
            listOf(
                DrawerItemModel(
                    R.id.action_splashFragment_to_logInFragment,
                    getString(strings.my_profile)
                ),
                DrawerItemModel(
                    R.id.action_splashFragment_to_logInFragment,
                    getString(strings.my_posts)
                ),
                DrawerItemModel(
                    R.id.action_splashFragment_to_logInFragment,
                    getString(strings.about)
                ),
                DrawerItemModel(
                    R.id.action_splashFragment_to_logInFragment,
                    getString(strings.my_cart)
                )
            )
        )

        binding.drawerRecycler.adapter = adapter
        binding.drawerRecycler.layoutManager = LinearLayoutManager(requireContext())
        adapter.openFragment = {
//            findNavController().navigate(it)
            val navController = requireActivity().supportFragmentManager.findFragmentById(R.id.fragmentContainerView) as NavHostFragment
            navController.navController.navigate(R.id.action_global_logInFragment)

        }
    }
}