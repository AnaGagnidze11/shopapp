package com.example.shopapp.screens.home.bottom_nav.favorites

import com.example.shopapp.BaseFragment
import com.example.shopapp.databinding.FavoritesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoritesFragment : BaseFragment<FavoritesFragmentBinding>(FavoritesFragmentBinding::inflate) {

    override fun setUpFragment() {

    }


}