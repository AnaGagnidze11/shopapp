package com.example.shopapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shopapp.databinding.DraweerItemLayoutBinding
import com.example.shopapp.models.DrawerItemModel

typealias OpenFragment = (actionId: Int) -> Unit
class DrawerRecyclerViewAdapter(private val items: List<DrawerItemModel>): RecyclerView.Adapter<DrawerRecyclerViewAdapter.ItemViewHolder>() {

    lateinit var openFragment: OpenFragment

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(
        DraweerItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size

    inner class ItemViewHolder(private val binding: DraweerItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var item: DrawerItemModel
        fun bind(){
            item = items[adapterPosition]
            binding.itemTitleTxtV.text = item.itemName
            binding.itemTitleTxtV.setOnClickListener {
                openFragment.invoke(item.id)
            }
        }
    }
}