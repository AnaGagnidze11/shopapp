package com.example.shopapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shopapp.databinding.PhotoLayoutBinding
import com.example.shopapp.extensions.setImageUrl
import com.example.shopapp.models.Post

class ViewPagerAdapter(private val photos: MutableList<Post.ImageItem>): RecyclerView.Adapter<ViewPagerAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ItemViewHolder(
        PhotoLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = photos.size

    inner class ItemViewHolder(private val binding: PhotoLayoutBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(){
            binding.imageView.setImageUrl(photos[adapterPosition].url)
        }
    }
}