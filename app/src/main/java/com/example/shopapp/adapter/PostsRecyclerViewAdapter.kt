package com.example.shopapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.shopapp.databinding.PostItemLayoutBinding
import com.example.shopapp.extensions.showIf
import com.example.shopapp.models.Post

class PostsRecyclerViewAdapter(): RecyclerView.Adapter<PostsRecyclerViewAdapter.PostViewHolder>() {

    private val posts: MutableList<Post> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PostViewHolder(
        PostItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = posts.size


    inner class PostViewHolder(private val binding: PostItemLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var model: Post
        private lateinit var photoAdapter: ViewPagerAdapter
        fun bind(){
            model = posts[adapterPosition]
            initPhotoAdapter()
            setData()

        }
        private fun setData(){
            binding.tvTitle.text = model.title
            binding.tvDesc.text = model.description
            binding.tvType.text = model.tags
            binding.tvPrice.text = "$ ${model.price.toString()}"
            binding.btnAddToCart.authButton.text = "Add To Cart"
        }

        private fun initPhotoAdapter(){
            photoAdapter = model.urls?.let { ViewPagerAdapter(it.toMutableList()) }!!
            binding.postsViewPager.adapter = photoAdapter
            binding.postsViewPager.showIf(model.urls.isNullOrEmpty())
            val size: Int = model.urls?.size ?: 0
            binding.imgBRightArrow.showIf(size<2)
            binding.imgBLeftArrow.showIf(size<2)
            binding.tvImagePage.showIf(size<2)
            if (size >= 2)
                setCurrentPageIndex(1)

            binding.postsViewPager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    setCurrentPageIndex(position+1)
                }
            })
        }

        private fun setCurrentPageIndex(index:Int){
                binding.tvImagePage.text = "$index/${model.urls?.size}"
        }
    }

    fun getPosts(posts: MutableList<Post>){
        this.posts.clear()
        this.posts.addAll(posts)
        notifyDataSetChanged()
    }
}