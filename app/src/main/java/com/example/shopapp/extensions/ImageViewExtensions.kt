package com.example.shopapp.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.shopapp.R

fun ImageView.setImageUrl(url: String?){
    if (!url.isNullOrEmpty())
        Glide.with(context).load(url).placeholder(R.drawable.ic_outline_image_not_supported_24).into(this)
    else
        setImageResource(R.drawable.ic_outline_image_not_supported_24)
}