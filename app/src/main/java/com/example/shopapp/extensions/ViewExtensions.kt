package com.example.shopapp.extensions

import android.app.Dialog
import android.view.View
import android.view.Window
import android.view.WindowManager


fun View.gone(){
    this.visibility = View.INVISIBLE
}

fun View.show(){
    this.visibility = View.VISIBLE
}

fun View.goneIf(show: Boolean){
    if (show){
        show()
    }else{
        gone()
    }
}

fun View.showIf(isGone: Boolean){
    if(isGone){
        gone()
    }else{
        show()
    }
}

fun Dialog.setUp(dialogView: Int){
    window!!.setBackgroundDrawableResource(android.R.color.transparent)
    window!!.requestFeature(Window.FEATURE_NO_TITLE)
    setContentView(dialogView)
    window!!.attributes.width = WindowManager.LayoutParams.MATCH_PARENT
    window!!.attributes.height = WindowManager.LayoutParams.WRAP_CONTENT
}