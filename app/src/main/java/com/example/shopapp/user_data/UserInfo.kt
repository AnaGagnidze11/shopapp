package com.example.shopapp.user_data

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserInfo @Inject constructor(@ApplicationContext context: Context){
    companion object {
        const val HAS_SESSION = "HAS_SESSION"
        const val TOKEN = "TOKEN"
    }

    private val sharedPreference: SharedPreferences by lazy {
        context.getSharedPreferences("user_details", Context.MODE_PRIVATE)
    }


    fun saveSession(session: Boolean) {
        sharedPreference.edit().putBoolean(HAS_SESSION, session).apply()
    }

    fun hasSession() = sharedPreference.getBoolean(HAS_SESSION, false)

    fun saveToken(token: String){
        sharedPreference.edit().putString(TOKEN, token).apply()
    }

    fun token() = sharedPreference.getString(TOKEN, "")
}