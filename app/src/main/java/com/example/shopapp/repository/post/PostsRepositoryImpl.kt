package com.example.shopapp.repository.post

import com.example.shopapp.models.LogInModel
import com.example.shopapp.models.Post
import com.example.shopapp.models.ReturnedError
import com.example.shopapp.network.PostsService
import com.example.shopapp.network.ResultControl
import com.google.gson.Gson

class PostsRepositoryImpl(private val postsService: PostsService): PostsRepository {
    override suspend fun getPosts(): ResultControl<List<Post>> {
        return try {
            ResultControl.loading<Post>(true)
            val result = postsService.getPosts()
            if (result.isSuccessful){
                ResultControl.success(result.body()!!)
            }else{
                val errorModel = Gson().fromJson(result.errorBody()!!.string(), ReturnedError::class.java)
                ResultControl.error(errorModel.error)
            }
        }catch (e: Exception){
            ResultControl.error(e.message.toString())
        }
    }
}