package com.example.shopapp.repository.post

import com.example.shopapp.models.LogInModel
import com.example.shopapp.models.Post
import com.example.shopapp.network.ResultControl

interface PostsRepository {

    suspend fun getPosts(): ResultControl<List<Post>>
}
