package com.example.shopapp.repository

import com.example.shopapp.models.LogInModel
import com.example.shopapp.models.ReturnedError
import com.example.shopapp.models.SignUpModel
import com.example.shopapp.network.AuthService
import com.example.shopapp.network.ResultControl
import com.google.gson.Gson
import javax.inject.Inject

class LogInRepositoryImpl @Inject constructor(private val authService: AuthService): LogInRepository {


    override suspend fun logIn(email: String, password: String): ResultControl<LogInModel> {
        return try {
            ResultControl.loading<LogInModel>(true)
            val result = authService.logIn(email, password)
            if (result.isSuccessful){
                ResultControl.success(result.body()!!)
            }else{
                val errorModel = Gson().fromJson(result.errorBody()!!.string(), ReturnedError::class.java)
                ResultControl.error(errorModel.error)
            }
        }catch (e: Exception){
            ResultControl.error(e.message.toString())
        }
    }

    override suspend fun signUp(
        email: String,
        password: String,
        username: String
    ): ResultControl<SignUpModel> {
        return try {
            ResultControl.loading<SignUpModel>(true)
            val result = authService.signUp(email, password, username)
            if (result.isSuccessful){
                ResultControl.success(result.body()!!)
            }else{
                val errorModel = Gson().fromJson(result.errorBody()!!.string(), ReturnedError::class.java)
                ResultControl.error(errorModel.error)
            }
        }catch (e: Exception){
            ResultControl.error(e.message.toString())
        }
    }
}