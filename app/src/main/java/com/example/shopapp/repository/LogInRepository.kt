package com.example.shopapp.repository

import com.example.shopapp.models.LogInModel
import com.example.shopapp.models.SignUpModel
import com.example.shopapp.network.ResultControl

interface LogInRepository {

    suspend fun logIn(email: String, password: String): ResultControl<LogInModel>

    suspend fun signUp(email: String, password: String, username: String): ResultControl<SignUpModel>
}