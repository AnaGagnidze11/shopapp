package com.example.shopapp.di

import androidx.viewbinding.BuildConfig
import com.example.shopapp.network.AuthService
import com.example.shopapp.network.PostsService
import com.example.shopapp.repository.LogInRepository
import com.example.shopapp.repository.LogInRepositoryImpl
import com.example.shopapp.repository.post.PostsRepository
import com.example.shopapp.repository.post.PostsRepositoryImpl
import com.example.shopapp.user_data.UserInfo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AuthModule {

    companion object {
        const val BASE_URL = "https://ktorhighsteaks.herokuapp.com/"
    }


    private fun httpClient(userInfo: UserInfo): OkHttpClient{
        val builder = OkHttpClient.Builder().addInterceptor(Interceptor { chain ->
            chain.request().url
            val request = chain.request().newBuilder()
            val token = userInfo.token()
            if (!userInfo.token().isNullOrBlank()){
                request.addHeader("Authorization", "Bearer $token")
            }
            val response = chain.proceed(request.build())
            response
        })

        if (BuildConfig.BUILD_TYPE == "debug"){
            builder.addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
        }
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideLogInBuilder(userInfo: UserInfo): AuthService =
        Retrofit.Builder().baseUrl(BASE_URL).client(httpClient(userInfo))
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(AuthService::class.java)

    @Provides
    @Singleton
    fun providePostsBuilder(userInfo: UserInfo): PostsService =
        Retrofit.Builder().baseUrl(BASE_URL).client(httpClient(userInfo))
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(PostsService::class.java)

    @Provides
    @Singleton
    fun provideForLogInInterface(authService: AuthService): LogInRepository =
        LogInRepositoryImpl(authService)

    @Provides
    @Singleton
    fun provideForPostInterface(postsService: PostsService): PostsRepository =
        PostsRepositoryImpl(postsService)

}