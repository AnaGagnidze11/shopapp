package com.example.shopapp.network

import com.example.shopapp.models.Post
import retrofit2.Response
import retrofit2.http.GET

interface PostsService {
    @GET("posts")
    suspend fun getPosts(): Response<List<Post>>
}