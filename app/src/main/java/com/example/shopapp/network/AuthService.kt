package com.example.shopapp.network

import com.example.shopapp.models.LogInModel
import com.example.shopapp.models.SignUpModel
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthService {

    @POST("login")
    @FormUrlEncoded
    suspend fun logIn(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<LogInModel>

    @POST("register")
    @FormUrlEncoded
    suspend fun signUp(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("full_name") fullName: String
    ): Response<SignUpModel>
}